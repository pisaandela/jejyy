package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.MoveCar;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MoveCarRepository extends JpaRepository<MoveCar, Long> {

    List<MoveCar> findByCommunityIdAndCreatedAtBetween(long communityId, long start, long end);

}
