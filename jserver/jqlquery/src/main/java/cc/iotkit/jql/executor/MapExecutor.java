package cc.iotkit.jql.executor;

import cc.iotkit.jql.ObjData;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;
import java.util.Map;

public class MapExecutor implements IExecutor<ObjData, ObjData> {

    public ObjData executor(JdbcTemplate jdbcTemplate, String sql, Class<ObjData> elementCls, Map<String, Object> params) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        List<Map<String, Object>> list = namedParameterJdbcTemplate.query(sql, params,
                new ColumnMapRowMapper()
        );

        if (list.size() == 0) {
            return null;
        }

        Map<String, Object> map = list.get(0);
        return new ObjData(map);
    }

}
