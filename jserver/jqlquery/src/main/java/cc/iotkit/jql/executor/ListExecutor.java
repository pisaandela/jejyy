package cc.iotkit.jql.executor;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;
import java.util.Map;

public class ListExecutor<T> implements IExecutor<T, List<T>> {

    public List<T> executor(JdbcTemplate jdbcTemplate, String sql, Class<T> elementCls, Map<String, Object> params) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        return namedParameterJdbcTemplate.query(sql, params,
                new BeanPropertyRowMapper<>(elementCls)
        );
    }

}
