package cn.chowa.ejyy.common;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;

import java.util.HashMap;

/**
 * 请求参数
 */
public class RequestData extends HashMap<String, Object> {

    public long getId() {
        String id = String.valueOf(get("id"));
        return Convert.toLong(id, 0L);
    }

    public long getCommunityId() {
        String id = String.valueOf(get("community_id"));
        checkParam(id, true, "^\\d+$");
        return Convert.toLong(id);
    }

    public int getPageNum() {
        return getInt("page_num", true, "^\\d+$");
    }

    public int getPageSize() {
        return getInt("page_size", true, "^\\d+$");
    }

    public <T> T getData(Class<T> cls) {
        return BeanUtil.toBeanIgnoreCase(this, cls, true);
    }

    public String getJsonStr(String name) {
        Object obj = get(name);
        return obj != null ? JSONUtil.toJsonStr(obj) : "";
    }

    public String getJsonStr(String name, boolean required) {
        Object obj = get(name);
        String str = obj != null ? JSONUtil.toJsonStr(obj) : "";
        checkParam(str, required, "");
        return str;
    }

    public String getStr(String name) {
        Object obj = get(name);
        return obj == null ? null : String.valueOf(get(name));
    }

    public String getStr(String name, boolean required, String validReg) {
        String val = String.valueOf(get(name));
        checkParam(val, required, validReg);
        return val;
    }

    public int getInt(String name) {
        return Convert.toInt(get(name), 0);
    }

    public int getInt(String name, boolean required, String validReg) {
        return getInt(name, required, validReg, 0);
    }

    public int getInt(String name, boolean required, String validReg, int def) {
        Object objVal = get(name);
        String val = objVal == null ? null : String.valueOf(get(name));
        checkParam(val, required, validReg);
        return Convert.toInt(val, def);
    }

    public boolean getBool(String name) {
        return getBool(name, false, null);
    }

    public boolean getBool(String name, boolean required, String validReg) {
        Object val = get(name);
        checkParam(val.toString(), required, validReg);
        if (val instanceof Boolean) {
            return (boolean) val;
        } else if (val instanceof Integer) {
            return ((Integer) val) != 0;
        }
        return false;
    }

    public long getLong(String name) {
        return Convert.toLong(get(name), 0L);
    }

    public long getLong(String name, boolean required, String validReg) {
        String val = String.valueOf(get(name));
        checkParam(val, required, validReg);
        return Convert.toLong(val, 0L);
    }


    public float getFloat(String name) {
        return Convert.toFloat(get(name), 0f);
    }

    public float getFloat(String name, boolean required, String validReg) {
        String val = String.valueOf(get(name));
        checkParam(val, required, validReg);
        return Convert.toFloat(val, 0f);
    }

    public boolean getBool(String name, boolean required) {
        String str = getStr(name);
        checkParam(str, required, "");
        return Convert.toBool(str);
    }

    private void checkParam(String val, boolean required, String validReg) {
        if (required && StrUtil.isBlank(val)) {
            throw new CodeException(Constants.code.PARAMS_ERROR, "参数不能为空");
        }

        if (val != null && StrUtil.isNotBlank(validReg) && !val.matches(validReg)) {
            throw new CodeException(Constants.code.PARAMS_ERROR, "参数错误");
        }
    }

}
