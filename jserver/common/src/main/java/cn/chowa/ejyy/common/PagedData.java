package cn.chowa.ejyy.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class PagedData<T> {

    private List<T> list;

    private long total;

    @JsonProperty("page_amount")
    private int pageAmount;

    @JsonProperty("page_num")
    private int pageNum;

    @JsonProperty("page_size")
    private int pageSize;

    public int getPageAmount() {
        return (int) Math.ceil((total * 1.0) / pageSize);
    }

    public static <T> PagedData<T> of(int pageNum, int pageSize, List<T> list, long total) {
        PagedData<T> pagedData = new PagedData<>();
        pagedData.setPageNum(pageNum);
        pagedData.setPageSize(pageSize);
        pagedData.setList(list);
        pagedData.setTotal(total);
        return pagedData;
    }
}
