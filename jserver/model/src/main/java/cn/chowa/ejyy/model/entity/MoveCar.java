package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ejyy_move_car")
public class MoveCar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty("community_id")
    private long communityId;

    @JsonProperty("wechat_mp_user_id")
    private Long wechatMpUserId;

    @JsonProperty("car_number")
    private String carNumber;

    @JsonProperty("move_reason")
    private int moveReason;

    @JsonProperty("live_img")
    private String liveImg;

    @JsonProperty("subscribed")
    private int subscribed;

    @JsonProperty("have_concat_info")
    private int haveConcatInfo;

    @JsonProperty("response_user_id")
    private Long responseUserId;

    @JsonProperty("response_content")
    private String responseContent;

    @JsonProperty("responsed_at")
    private Long responsedAt;

    @JsonProperty("created_at")
    private long createdAt;
}
