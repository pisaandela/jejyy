package cn.chowa.ejyy.user;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.common.utils.AuthUtil;
import cn.chowa.ejyy.dao.PropertyCompanyUserRepository;
import cn.chowa.ejyy.dao.UserQuery;
import cn.chowa.ejyy.model.entity.PropertyCompanyUser;
import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.util.SaResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName reset
 * @Description TODO
 * @Author ironman
 * @Date 15:57 2022/8/17
 */

@Slf4j
@RestController
@RequestMapping("/pc/user")
public class reset {

    @Autowired
    private UserQuery userQuery;

    @Autowired
    private PropertyCompanyUserRepository userRepository;

    /*
     * 重置用户密码
     **/
    @SaCheckRole(Constants.RoleName.RLZY)
    @VerifyCommunity(true)
    @PostMapping("/reset")
    public SaResult reset(@RequestBody RequestData data) {

        //执行 相关操作
        String password = data.getStr("password",true, "(.{6,32})");
        //md5加密
        String hashedPwd1 = DigestUtils.md5DigestAsHex((password).getBytes());
        //查询用户信息，并更新密码
        String userName = AuthUtil.getUserId();
        ObjData userInfo = userQuery.getUserInfo(Constants.status.FALSE, userName);
        //更新数据库用户密码
        userRepository.saveOne(userInfo.getLong("id"),hashedPwd1);
        //测试返回自定义结果
        SaResult res = new SaResult();
        res.setCode(Constants.code.SUCCESS);
        res.setMsg("重置登录密码成功");
        return res;
    }
}
