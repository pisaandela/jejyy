package cn.chowa.ejyy.user;

import cn.dev33.satoken.stp.StpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/pc/user")
public class state {

    /**
     * 获取用户状态
     */
    @GetMapping("/state")
    public Map<?, ?> getState() {
        String token = StpUtil.getTokenInfo().getTokenValue();
        return Map.of(
                "state", token,
                "expire", StpUtil.getSessionTimeout()
        );
    }
}
